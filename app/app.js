'use strict';

// =================================================================================
// App Configuration
// =================================================================================

const {App} = require('jovo-framework');
var request = require('request');


const config = {
    logging: true,
};

const app = new App(config);

app.setHandler({
    'LAUNCH': function() {
        this.ask('Bonjour, en quoi puis-je vous aider ?', 'Pardon, je n\'ai pas compris la question. Pouvez-vous répéter ?');
    },

    'StationIntent': function(stop) {
        var alexa = this;
        request.get({url: 'https://api.ginko.voyage/TR/getTempsLieu.do', qs: { "nom" : stop.value }}, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var responseObj = JSON.parse(body);
                if (responseObj.ok && responseObj.objets.listeTemps.length > 0) {
                    var stopDestination = [];
                    var messageRetour = "Voici les prochains départs à l'arrêt " + stop.value + ". ";
                    responseObj.objets.listeTemps.forEach(element => {
                        if (stopDestination.indexOf(element.destination) === -1) stopDestination.push(element.destination);
                    });

                    var filterStops = function (search, keys) {
                        var lowSearch = search.toLowerCase();
                        return responseObj.objets.listeTemps.filter(function(wine){
                            return keys.some( key => 
                                String(wine[key]).toLowerCase().includes(lowSearch) 
                            );
                        });
                    }

                    stopDestination.forEach(destination => {
                        let stops = filterStops(destination, ['destination']); 

                        if (stops.length > 0) {
                            messageRetour += destination + ' : ';

                            let stopTimes = [];
                            let stopTimesMsg;
                            stops.forEach(stop => {
                                stopTimes.push(parseInt(stop.temps.replace(' min', '')));
                                stopTimes = stopTimes.sort((a, b) => a - b);
                                stopTimesMsg = stopTimes.join(', ');
                                stopTimesMsg = stopTimesMsg.replace(/,(?=[^,]*$)/, ' et');
                                // messageRetour += stop.temps.replace(' min', ',');
                            });
                            messageRetour += stopTimesMsg + ' minutes.';
                            console.log(stopTimesMsg);
                            
                        }
                    });

                    // console.log(filterStops('101', ['idLigne']));
                    // alexa.tell('YOUPI');

                    alexa.tell(messageRetour);
                } else {
                    alexa.ask('Aucun résultat pour ' + stop.value + '. Essayez avec un autre nom d\'arrêt.');

                }

            } else {
                alexa.tell('Erreur lors de la requête.');
            }
        })
    },
    'AMAZON.CancelIntent': function() {
        this.ask('Veuillez poser votre question', "Pardon, je n'ai pas entendu. Pouvez-vous répéter ?");
    },
});

module.exports.app = app;
