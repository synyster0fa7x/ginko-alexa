# Alexa Demo with Ginko API

## Installation

```
git clone git@gitlab.com:synyster0fa7x/ginko-alexa.git
cd ginko-alexa/
npm install -g jovo
```
Then, configure your app in amazon developer console, then run `jovo run`.